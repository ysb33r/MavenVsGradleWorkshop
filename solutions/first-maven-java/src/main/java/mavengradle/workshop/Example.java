package mavengradle.workshop;

class Example {
    public void setName(final String nm) {
        name = nm;
    }

    public String getName() {
        return name;
    }

    private String name;
}