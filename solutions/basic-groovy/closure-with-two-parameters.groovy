def renameJpeg = { String s, File f ->
    new File(f.absolutePath.replaceAll (~/\.jpeg/, s))
}