= Installation


== Classic methods

* Maven can be downloaded from http://maven.apache.org/download.html["Maven download site"].
* Gradle can be downloaded from https://gradle.org/gradle-download/["Gradle download site"].

Manual installation of both tools roughly follows a well-known procedure:

[source,bash]
----
wget -nd -np ${ROOT_URL}/${maven_or_gradle}-${VERSION}.zip  <1> <2>
unzip ${maven_or_gradle}-${VERSION}.zip <3>
export PATH=$PATH:${maven_or_gradle}-${VERSION}/bin <4>
----
<1> Use `apache-maven` or `gradle`
<2> Select the required version
<3> Unpack the archive somewhere
<4> Set the execution PATH to include the installed build tool

Various distributions or package managers allow installation of both Gradle & Maven as
  global packages. This is not a scalable or flexible solution for development:

* Only one version of the tool is dominant.
* Editing the PATH can be a tedious (and possibly erroneous task).
* Changing between version of the same tool is unnecessary manual work.
* Versions supported by the package manager can be out of date.

Next: link:sdkman.html[Manage installations with SDKMan]


