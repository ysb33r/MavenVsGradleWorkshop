= First Java Project with Gradle

.Core build definition is `build.gradle`
[source,groovy]
----
include::{solutions}/first-gradle-java/build.gradle[]
----
<1> Java projects are activated via the built-in 'java' plugin.
<2> Local & remote repositories are defined in one or more `repositories {}` blocks. Unlike Maven, which automatically
 adds Maven Central, Gradle explicitly requires all repositories to be defined. (Some plugins add
 their own default repositories).
<3> The `init` task will add `jcenter()` as a repository. This cloud-based repository serves everything
  from Maven Central plus a load more from JCenter-associated Bintray repositories.
<4> Dependencies are declared in one or more `dependencies {}` blocks.
<5> Dependencies normally follow the format of a `configurationName dependencyNotation`. Configurations
  are loosely related to Maven scopes, but can achieve a lot more. There are no default configurations.
  The `testCompile` configuration is one of seven configurations injected by the `java` plugin.

* Unlike Maven, Gradle does not support archetypes.
** Similar behaviour, although not as extensive as Maven archetypes, via `./gradlew init --type X` and external `lazybones` tool.

== Java Configurations

The `java` plugin injects the following configurations:

* *compile:* Dependencies needed for compilation
* *compileOnly:* Dependencies needed only for compilation, but not runtime or testing. Does not
  appear in transitive dependencies. Can be used similar to Maven's `provided` scope, but is not
  an exact equivalent.
* *runtime:* Dependencies needed for running the application or using the artifact.
* *testCompile:* Dependencies needed for compiling tests. `testCompile` extends from `compile`.`
* *testCompileOnly:* Dependencies only needed for compiling test, but not running tests.
* *testRuntime:* Dependencies needed wen running tests.
* *archives*: Typically only used by publications, such as adding javadoc * source JARs.

NOTE: If Maven's `provided` & `optional` behaviour is required then use the
  https://plugins.gradle.org/plugin/nebula.provided-base[nebula.provided-base] and
  https://plugins.gradle.org/plugin/nebula.optional-base[nebula.provided-optional] plugins.

'''

== Gradle Phases & Lifecycles

Gradle & Maven differ on terminology, but at a high-level intent is the same for both. A Maven goal and Maven lifecycle phase
  (with the possible exception of `validation`) is essentially the same as a Gradle task.

Implementation-wise the difference is between Gradle & Maven is strongly visible.
Gradle is based on a directed acyclic graph of tasks which do the work.
Maven is a model of fixed, linear phases to goals are attached. This should not detract technology
people of switching over from one to the other.

Gradle essentially has three phases:

* Initialisation: Determines which projects are taking part in the build.
* Evaluation: Evaluates & validates all build scripts and creates a task execution graph.
* Execution: Executes the task graph.

Gradle only has a few built-in setup & help tasks. These tasks can be seen by running `gradle` on an empty or
  non-existent `build.gradle` file. The rest of the tasks are added via plugins. There are certain lifecycle tasks
  that are indirectly created when many plugins are applied. These are:

* *assemble:* Compiles everything and creates the packages.
* *clean:* Removes all artifacts. (by default this simply means removing the `build` directory).
* *check:* Runs all test tasks.
* *build:* Build, package and test everything.

.Java project with Gradle exercise
[sidebar]
****
* Change to `exercises/first-gradle-java` directory. (You'll notice that `src` folder has already
  been created for you to save time).
* Run `gradle wrapper --gradle-version 3.2.1 init --type java-library`
* Inspect the `build.gradle` and `settings.gradle` files that were created.
* Remove the `src/main/java/Library.java` and `src/test/java/LibraryTest.java` files that were created
  by the `init` task. (We don't need them and will use the `Example*.java` files instead).
* Run `./gradlew dependencies` and look at the output.
* Remove the dependency line for `slf4j` that were added. (It is not needed anymore for this exercise).
* Run `./gradlew dependencies` again and look at the output.
* Build the product by running `./gradlew build`. Take note of the tasks that were executed.
* Run `./gradlew tasks` and `./gradlew tasks --all`. Notice all of the tasks added by the `java` plugin.
* Now add a test that will fail. Run `./gradlew test` and open the report (`build/reports/tests/test/index.html`) in your browser
****



* Controlling tests and report format - ???

== Further Reading

* Gradle Lifecycle - https://docs.gradle.org/current/userguide/build_lifecycle.html
* Gradle Test DSL - https://docs.gradle.org/3.0/dsl/org.gradle.api.tasks.testing.Test.html

Next: link:groovy.html[Groovy Basics]